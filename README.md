# **Penn State Physics & Nanoscale Materials REU Summer Research Project 2022 of Haley Steuber May 29 - .**

## Inference of dark matter in neutron star cores from gravitational wave observations
   -----------------------------------------------------------------------------------


## Software use
We use Bayesian inference techniques to measure the tidal deformability parameter of individual BNS systems through the [Bilby software](https://arxiv.org/pdf/1811.02042.pdf)  


## Acknowledgments
Thank you to 

Divya Singh - Grad mentor

Dr B.S. Sathyaprakash - Professor

## Project status
As of August 3, 2022, injected sets with updated 'luminosity distance' to a redshift of z = 0.05 at 45 Mpc to correct SNR value.


## Files
[Posterior distribution of masses][1]

[1]: https://rawgit.com/Shteuber/penn-state-summer-project/-/d41101b0cee0241b05cae48e090caa9f45515f9f/production_runs_scripts/M1M2.prior.png

***

