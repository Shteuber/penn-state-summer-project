#!/usr/bin/env python
# coding: utf-8

# In[76]:


import matplotlib as plt
import numpy as np
import matplotlib.cm as cm


# In[ ]:


# omega vs time plot


# In[77]:


def omega(t, t_c, f):
# omega function with respect to time, total frequency given 
# an initial time and the time the binary coalesces t_c
# w_i is the initial frequency
# w = w_i (1 - t/t_c)
    # frequency = 10
    return f * (1 - t / t_c)**(-3/8)


# In[78]:


def t_coalesce(f, mass):
# mass passed to the function is assumed to be a binary
# pair that is equal in mass thereby making the equation
# for chirp mass M = (mass ^3/5) * 2mass 
    return (5/256) * f**(-8/3) * (mass**(3/5))*(2*mass)


# In[81]:


res = 50
m1 = 0.25

t_min = np.log10(3600)
t_max = np.log10(31557600)
om_min = np.log10(1.3e-7)
om_max = np.log10(3e-4)

freq = np.logspace(om_min, om_max, res, endpoint = True)
time = np.logspace(t_min, t_max, res, endpoint = True)

freq_array = np.zeros(len(freq))
t_array = np.zeros(len(time))
om_array = np.zeros(len(freq))

j = 0
# for i in range(len(time)):
    # freq_array[j] = freq[i]
    # t_array[j] = time[i]
    # t = t_colesce(omega[i]) 
    # om_array[j] = omega(time, freq)
    
    # j += 1
    
# X = np.reshape(om_array[j])


# In[82]:


# frequency versus mass plot
#
#
#
eta = 0.25
time = np.logspace(np.log10(3600), np.log10(31557600), res)


# In[83]:


def frequency(eta, T, M):
    return ((5 / (192 * np.pi * eta * T**2)) * (3e8/ (6.674e-11 * M * 2 * np.pi))**(5/3))**(3/11)


# In[ ]:


def mass_total(m1):
    return (0.25 * m1)**(3/5) * 2 * m1


# In[ ]:




