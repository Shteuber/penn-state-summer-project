#!/usr/bin/env python
# coding: utf-8

# In[410]:


import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm


# In[411]:


def velocity(mass, radius):
# velocity function in m/s
    return np.sqrt((mass * 6.674e-11) / radius)


# In[412]:


def eta(m1, m2):
# unitless
# eta function
# if masses are equal, return 0.25
# if masses are not equal, calculate eta function
    if(m1 == m2):
        return 0.25
    else:
        return (m1 * m2) / np.power((m1 + m2), 2)


# In[413]:


def luminosity(velocity, eta):
# unites in J/s, Watts, erg/s
# luminosity function
# for velocities less than c
    numerator = 32 * np.power(3e8, 5) * eta**2
    denominator = 5 * 6.674e-11
    v_over_c = np.power((velocity / 3e8), 10)
    
    return (numerator / denominator) * v_over_c


# In[414]:


def velocity_c(velocity):
# unitless
# velocity / speed of light
    return velocity / 3e8


# In[415]:


def planet(mass):
# function takes in mass value and returns planet name

    if(mass == 0.33e24):
        return "Mercury"
    elif(mass == 4.867e24):
        return "Venus"
    elif(mass == 5.972e24):
        return "Earth"
    elif(mass == 0.65e24):
        return "Mars"
    elif(mass == 1900e24):
        return "Jupiter"
    elif(mass == 570e24):
        return "Saturn"
    elif(mass == 87e24):
        return "Uranus"
    elif(mass == 100e24):
        return "Neptune"
    else:
        return "compact object"


# In[416]:


(pMass, radius) = np.loadtxt("SolarSystemMassRadius.txt", 
                                  delimiter = ",", unpack = True)
num = 20

eta_array = np.zeros(num**2)
l_array = np.zeros(num**2)
p_array = np.zeros(num**2, dtype=str)
v_c_array = np.zeros(num**2)
# np.zeros((2,), dtype=[('x', 'i4'), ('y', 'i4')])

mSun = 1.988e30

j = 0

for i in range(len(pMass)):
    # print("Planet: {}".format(name[i]))
    eta_array[j] = eta(mSun, pMass[i])
    print("eta value: {}".format(eta_array[j]))
    l_array[j] = luminosity(velocity(pMass[i], radius[i]), eta(mSun, pMass[i]))
    print("luminosity value: {}".format(l_array[j]))
    p_array[j] = planet(pMass[i])
    print("planet: {}".format(p_array[j]))
    v_c_array[j] = velocity_c(velocity(pMass[i], radius[i]))
    print("v/c value: {}".format(v_c_array[j]))
    j += 1

# print(len(v_c_array))
# print(len(eta_array))

X = np.reshape(v_c_array, (num, num))
Y = np.reshape(eta_array, (num, num))
Z = np.reshape(l_array, (num, num))

plot, labels = plt.subplots()
# ax = plot.add_subplot(111, projection = '3d')
# ax.scatter(X, Y, Z)
plot = plt.contour(X, Y, Z)
plt.colorbar(label = "luminosity")
plt.show()


# labels.set_ylabel(r'$\eta$', fontsize = 20)
# labels.set_xlabel(r'$\frac{v}{c}$', fontsize = 20)

# fig= plt.figure()
# ax= fig.add_subplot(111, projection= '3d')
# ax.scatter(x,y,z)


# In[417]:


mEarth = 5.972e24
mSun = 1.988e30
rEarth = 1.5e11

velocityEarth = velocity(mEarth, rEarth)
print("Earth velocity: {}".format(velocityEarth))

# eta_array = eta(mSun, mEarth)
# print("Earth-Sun eta: {}".format(eta))

# luminosity = luminosity(velocityEarth, eta)
# print("Earth-Sun luminosity: {}".format(luminosity))

# v_over_c = velocity_c(velocityEarth)
# print("velocity over speed of light: {}".format(v_over_c))

