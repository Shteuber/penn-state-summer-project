#!/usr/bin/env python
# coding: utf-8

# In[221]:


import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm


# In[222]:


def luminosity(eta, vc):
    return (32/5) * (((3e8)**5 * eta**2) / 6.674e-11) * (vc)**(10)


# In[223]:


eta_min = np.log10(10e-2)
eta_max = np.log10(0.25)
v_c_min = np.log10(1.1)
v_c_max = np.log10(0.5)


# In[224]:


res = 50

eta = np.logspace(eta_min, eta_max, res, endpoint = True)
v_c = np.logspace(v_c_min, v_c_max, res, endpoint = True)

eta_array = np.zeros(len(eta)**2)
v_c_array = np.zeros(len(v_c)**2)
lum_array = np.zeros(len(eta) * len(v_c))

# print("eta array: {}".format(len(eta_array)))
# print("v_array: {}".format(len(v_array)))

k = 0
for i in range(len(eta)):
    eta_array[k] = eta[i]
    # print("eta: {}".format(eta[i]))
    v_c_array[k] = v_c[i]
    # print("v upon c: {}".format(v_c[i]))
    lum_array[k] = np.log10(luminosity(eta[i], v_c[i]))
    # print("luminosity: {}".format(luminosity(eta[i], v_c[i])))
    # print()
    k += 1
    
# print(v_c_array[39])
# print(eta[39])
X = np.reshape(v_c_array, (res, res))
Y = np.reshape(eta_array, (res, res))
Z = np.reshape(lum_array, (res, res))

figure, ax = plt.subplots()
figure = plt.contourf(X, Y, Z, cmap = cm.gnuplot2_r)
plt.colorbar(label = 'GW luminosity')
plt.show()

